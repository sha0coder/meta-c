/*
	C metaprogrammed
	by sha0@badchecksum.net
*/

#define VERSION 0.1

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/tcp.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

#define BUFF 2048
#define LINE 80
#define RECV_TIMEOUT 4
#define DEBUG 1


#define ERR(msg)  ({ printf("%s\n",msg); _exit(1); }) 
#define DISP(msg) printf("%s\n",msg)
#define DISPN(n)  printf("%d\n",n)
#define DBG(msg)  (DEBUG?DISP(msg):0)
#define DBGN(n)   (DEBUG?DISPN(n):0)
#define STR_LIMIT(buff,limit) (strlen(buff)>limit?buff[limit-1]=0:1)
#define EQ(a,b) (strcmp(a,b) == 0)

#define asm __asm__
#define TRAP asm("int $3")

#define APPEND(file,str) ({ \
	int fd = open(file,O_RDWR); \
	write(fd,str,strlen(str)); \
	close(fd); \
})

#define READ(file,buff,sz) ({ \
	int fd = open(file,O_RDONLY); \
	read(fd,buff,sz); \
	close(fd); \
})

#define READ_LINES(file,callback) ({ \
	FILE *f = fopen(file,"r"); \
	char line[100]; \
	int i=0; \
	while (fgets(line,100,f)) \
		callback(i++,line); \
	fclose(f); \
})

#define DIR(name,callback) ({ \
	DIR *dir; \
	struct dirent *dirent; \
	dir = opendir(name); \
	int i=0; \
	while (dirent = readdir(dir)) \
		callback(i++,dirent->d_name); \
	closedir(dir); \
})

#define FOREACH(i,arr,callback) ({ \
	for (i=0;arr[i];i++) \
		callback(i,arr[i]); \
})


#define SOCKOPT_RECV_TIMEOUT \
	struct timeval tv; \
	tv.tv_sec = RECV_TIMEOUT; \
	setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&tv,  sizeof tv);

#define SOCKOPT_TCP_NODELAY(on) setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (const char *)&on, sizeof(int))
 
#define CONNECT(host,port) \
	struct hostent *hp; \
	struct sockaddr_in addr; \
 	int sock; \
 	if((hp = gethostbyname(host)) == NULL) \
    	ERR("invalid host"); \
    memcpy(&addr.sin_addr, hp->h_addr, hp->h_length); \
    addr.sin_port = htons(port); \
	addr.sin_family = AF_INET; \
	sock = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP); \
	if(sock < 0) \
		ERR("socket error"); \
	SOCKOPT_RECV_TIMEOUT; \
	if(connect(sock, (struct sockaddr *)&addr, sizeof(struct sockaddr_in)) < 0) \
		ERR("can't connect"); \
	DBG("connected");

#define SEND(msg) send(sock,msg,strlen(msg),0)
#define RECV(buff,sz) recv(sock,buff,sz,0)
#define CLOSE_CONNECTION shutdown(sock, SHUT_RDWR)

//TODO: pasar a scanf-regex
#define URL(u)  \
	char url[255]; \
	char host[80]; \
	char path[255]; \
	char *p; \
	int port; \
	int res; \
	strncpy(url,u,254); \
	p = strchr(url,':')+1; \
	if (strchr(p,':') == NULL) { \
		DBG("yepa"); \
		sscanf(url, "http://%99[^/]%99[^\n]", host, path); \
		port = 80; \
	} else \
		sscanf(url, "http://%99[^:]:%99d%99[^\n]", host, &port, path); 



#define GET(url,callback) ({ \
	int i; \
	char buff[BUFF]; \
	URL(url); \
	DBG(host); \
	CONNECT(host,port); \
	snprintf(buff,BUFF,"GET %s HTTP/1.1\nHost: %s\nUser-Agent: c\n\n",path,host); \
	DBG(buff); \
	SEND(buff); \
	memset(buff,0,BUFF); \
	RECV(buff,BUFF); \
	callback(buff); \
	CLOSE_CONNECTION; \
})

#define GETH(url,hdr,callback) ({ \
	int i; \
	char buff[BUFF]; \
	URL(url); \
	DBG(host); \
	CONNECT(host,port); \
	snprintf(buff,BUFF,"GET %s HTTP/1.1\nHost: %s\nUser-Agent: c\n%s\n\n",path,host,hdr); \
	DBG(buff); \
	SEND(buff); \
	memset(buff,0,BUFF); \
	RECV(buff,BUFF); \
	callback(buff); \
	CLOSE_CONNECTION; \
})

#define GET_CHUNKED(url,callback) ({ \
	int i; \
	char buff[BUFF]; \
	URL(url); \
	CONNECT(host,port); \
	snprintf(buff,BUFF,"GET %s HTTP/1.1\nHost: %s\nUser-Agent: c\n\n",path,host); \
	DBG(buff); \
	SEND(buff); \
	memset(buff,0,BUFF); \
	while (RECV(buff,BUFF)) { \
		DBG("incoming data"); \
		callback(i++,buff); \
	} \
	CLOSE_CONNECTION; \
})

struct http_response {
	int code;
	char msg[LINE];
	char server[LINE];
	char location[LINE];
	char html[BUFF];
};

#define POSTM(url,cook,filename,filecontent) ({ \
	char buff[BUFF];
	char post[BUFF];
	URL(url); \
	CONNECT(host,port); \
	snprintf(buff,BUFF,"POST %s HTTP/1.1\nHost: %s\nUser-Agent: c\nContent-Type: multipart/form-data; boundary=---------------------------14644506381775945704227237905\n\n",path,host); \
	snprintf(buff,BUFF,"\n\n",filename,filecontent); \
})


#define HTTP_PARSE(data) \
	struct http_response resp; \
	char hdr[LINE]; \
	char val[LINE]; \
	char *p = strtok(data,"\n"); \
	STR_LIMIT(p,LINE); \
	DBG(p); \
	sscanf(p,"HTTP/1.1 %d %s",resp.code,resp.msg); \
	while ((p=strtok(NULL,"\n")) != NULL) { \
		STR_LIMIT(p,LINE); \
		DBG(p); \
		if (sscanf(p,"%s: %s",hdr,val) >0) { \
			if (EQ(hdr,"Server")) \
				strcpy(resp.server,val); \
			if (EQ(hdr,"Location")) \
				strcpy(resp.location,val); \
		} \
	}




#define DATAGRAM(host,port,data) ({ \
	int sock = socket (AF_INET, SOCK_DGRAM, 0); \
})


/*
	TODO:
		- system/popen
		- linked list
		

*/
